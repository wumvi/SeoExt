/**
 *
 * @param {Bg} bg
 * @constructor
 */
function BgTabController(bg) {
    /**
     * 
     * @type {Bg}
     * @private
     */
    this.bg_ = bg;

    this.init_();
}

BgTabController.prototype.init_ = function () {


    this.initEvent_();
};


BgTabController.prototype.initEvent_ = function () {
    console.log('BgTabController content');
};
