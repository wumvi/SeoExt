/**
 *
 * @param {Bg} bg
 * @constructor
 * @implements {BgLogicInterface}
 */
function KabaneriBg(bg) {
    /**
     *
     * @type {Bg}
     * @private
     */
    this.bg_ = bg;
}

KabaneriBg.BASE_YANDEX_URL = 'https://ya.ru/';
KabaneriBg.PATH_CONTENT_SCRIPT_PREFIX = 'Logic/Kabaneri/Content/';

/**
 * @inheritDoc
 */
KabaneriBg.prototype.run = function () {
    chrome.tabs.create({url: KabaneriBg.BASE_YANDEX_URL});
};


/**
 * @inheritDoc
 */
KabaneriBg.prototype.getInitPageUrl = function (portTab) {
    var url = portTab.sender.url;
    if (/https?:\/\/ya\.ru\//.test(url)) {
        return KabaneriBg.PATH_CONTENT_SCRIPT_PREFIX + 'YandexContent.js';
    } else if (/^https?:\/\/yandex\.ru\/search/.test(url)) {
        return KabaneriBg.PATH_CONTENT_SCRIPT_PREFIX + 'YandexSerchContent.js';
    } else if (/https:\/\/xn--80aacrphyz.xn--p1ai/.test(url)) {
        return KabaneriBg.PATH_CONTENT_SCRIPT_PREFIX + 'MyContent.js';
    }

    return KabaneriBg.PATH_CONTENT_SCRIPT_PREFIX + 'EnemyContent.js';
};

/**
 * @this {Bg}
 */
this.onInitBgLogic_(new KabaneriBg(this));


