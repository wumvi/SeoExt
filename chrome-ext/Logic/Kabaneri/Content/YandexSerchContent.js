/**
 *
 * @param {Content} content
 * @constructor
 */
function YandexSerchContent(content){
    this.content_ = content;

    console.log('YandexSerchContent');

    this.flagAddContent_ = false;

    jQuery('.main__content').bind("DOMSubtreeModified", this.OnDOMSubtreeModified_.bind(this));
}

/**
 * 
 * @constructor
 * @private
 */
YandexSerchContent.prototype.OnDOMSubtreeModified_ = function(){
    if (!this.flagAddContent_) {
        return;
    }

    this.flagAddContent_ = !this.flagAddContent_;

    console.log(arguments);
};

/**
 * @this {Content}
 */
this.onInitCustomLogic(new YandexSerchContent(this));