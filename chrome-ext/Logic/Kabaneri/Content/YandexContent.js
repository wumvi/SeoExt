/**
 *
 * @param {Content} content
 * @constructor
 */
function YandexContent(content){
    this.content_ = content;

    console.log('YandexContent');

}

/**
 * @this {Content}
 */
this.onInitCustomLogic(new YandexContent(this));