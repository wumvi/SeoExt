/**
 *
 * @param {Content} content
 * @constructor
 */
function MyContent(content){
    this.content_ = content;

    console.log('my content');
}

/**
 * @this {Content}
 */
this.onInitCustomLogic(new MyContent(this));