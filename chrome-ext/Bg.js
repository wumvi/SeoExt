function Bg() {
    /**
     *
     * @type {Array.<ScreenResolution>}
     * @private
     */
    this.screenList_ = [];

    this.init_();
}

Bg.prototype.init_ = function () {
    this.screenList_.push(new ScreenResolution(1366, 768, 0, 50));
    this.screenList_.push(new ScreenResolution(1280, 1024, 51, 65));
    this.screenList_.push(new ScreenResolution(1280, 800, 66, 80));
    this.screenList_.push(new ScreenResolution(1920, 1080, 81, 100));

    var screen = this.getScreenResolution_();
    console.log(screen);

    /**
     *
     * @type {null|BgLogicInterface}
     * @private
     */
    this.bgLogic_ = null;

    this.initEvent_();
};


Bg.prototype.initEvent_ = function () {
    chrome.runtime.onConnect.addListener(this.onPageConnect_.bind(this));
};


/**
 *
 * @param {Port} portTab
 * @private
 */
Bg.prototype.onPageConnect_ = function (portTab) {
    var tabId = portTab.sender.tab.id;
    console.log('tabId =', tabId);
    portTab.onMessage.addListener(this.onPageMessage_.bind(this));
};


Bg.prototype.onPageConnect_ = function (portTab) {
    
};

/**
 *
 * @param {Object} msg
 * @param {Port} portTab
 * @private
 */
Bg.prototype.onPageMessage_ = function (msg, portTab) {
    var message = SendMessage.make(msg);

    // Если это пустая вкладка Chrome, просто выходим
    if (/chrome\/newtab/.test(portTab.sender.url)) {
        return;
    }

    switch (message.getAction()) {
        case MessageAction.MESSAGE_ACTION_INIT:
            // var url =  /** @type {string} */ (message.getData());
            console.log(portTab);
            this.onInitPage_(portTab);
            break;
    }
};


/**
 *
 * @param {Port} portTab
 * @private
 */
Bg.prototype.onInitPage_ = function (portTab) {
    // portTab.postMessage({'type':'init', data: tabInitParam[tabId]});
    var url = this.bgLogic_.getInitPageUrl(portTab);
    url = chrome.extension.getURL(url);
    if (!url) {
        return;
    }

    this.loadScript_(url, this.onInitPageDataDone_.bind(this, portTab));
};

/**
 *
 * @param {Port} portTab
 * @param {string} raw
 * @private
 */
Bg.prototype.onInitPageDataDone_ = function (portTab, raw) {
    portTab.postMessage(new SendMessage(MessageAction.MESSAGE_ACTION_INIT, raw));
};


/**
 *
 * @param {string} url
 * @param {Function} callback
 * @private
 */
Bg.prototype.loadScript_ = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            callback(xhr.responseText);
        }
    };

    try {
        xhr.send();
    } catch (z) {
        console.error(now(), 'Error[1]: Don\'t load', url, z);
        xhr.status = 0;
    }
};


/**
 *
 * @param {string} url
 * @private
 */
Bg.prototype.initPage_ = function (url) {
    this.loadScript_(url, this.onLoadBgLogic_.bind(this));
};

Bg.prototype.onLoadBgLogic_ = function (rawText) {
    eval(rawText);
};

/**
 *
 * @param {BgLogicInterface} bgLogic
 * @private
 */
Bg.prototype.onInitBgLogic_ = function (bgLogic) {
    this.bgLogic_ = bgLogic;
    this.bgLogic_.run();
};


/**
 *
 * @param {number} min
 * @param {number} max
 * @return {number}
 * @private
 */
Bg.prototype.getRandomInt_ = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

/**
 * Получаем случайное разрешение
 * @return {ScreenResolution} Модель с зарешением
 * @private
 */
Bg.prototype.getScreenResolution_ = function () {
    var rnd = this.getRandomInt_(0, 100);
    for (var num = 0; num < this.screenList_.length; num += 1) {
        if (this.screenList_[num].getBeginProc() <= rnd && rnd <= this.screenList_[num].getEndProc()) {
            return this.screenList_[num];
        }
    }

    return this.screenList_[0];
};


Bg.prototype.stopVnc_ = function () {
    console.info('Stop VPN');
};


Bg.prototype.changeResolution_ = function () {
    var screen = this.getScreenResolution_();
    console.info('changeResolution', screen);
};


/**
 *
 * @param {function|null} callback
 * @private
 */
Bg.prototype.clearDataInBrowser_ = function (callback) {
    chrome.browsingData.removeCookies({}, function () {
        chrome.browsingData.removeLocalStorage({}, function () {
            chrome.browsingData.removeIndexedDB({}, function () {
                chrome.browsingData.removePluginData({}, function () {
                    chrome.browsingData.removeWebSQL({}, function () {
                        chrome.browsingData.removeCache({}, function () {
                            chrome.browsingData.removeAppcache({}, function () {
                                chrome.browsingData.removeFormData({}, function () {
                                    chrome.browsingData.removeHistory({}, function () {
                                        chrome.browsingData.removeHistory({}, function () {
                                            chrome.browsingData.removeHistory({}, function () {
                                                chrome.browsingData.removeFileSystems({}, function () {
                                                    callback ? callback() : null;
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};

Bg.prototype.run = function (url) {
    var jsUrl = chrome.extension.getURL(url);
    this.initPage_(jsUrl);
};

Bg.escapeRegExp = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

var bg = new Bg();
