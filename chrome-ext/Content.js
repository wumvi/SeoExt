function Content() {
    /**
     *
     * @type {Port|null}
     * @private
     */
    this.port_ = null;

    this.init_();
}

Content.prototype.init_ = function () {

    var imgUrl = chrome.extension.getURL("images/point.png");
    // jQuery(document.body).append('<img src="'+imgUrl+'" style="position: fixed; left: 0px; top: 0px;z-index:1000" id="excur"/>');

    this.port_ = chrome.runtime.connect({});
    //

    this.port_.postMessage(new SendMessage(MessageAction.MESSAGE_ACTION_INIT));

    this.initEvent_();
};


Content.prototype.initEvent_ = function () {
    console.log('log content');

    this.port_.onMessage.addListener(this.onMessageFromBg_.bind(this));
};

Content.prototype.onMessageFromBg_ = function (msg) {
    var message = SendMessage.make(msg);
    var raw = /** @type {string} */ (message.getData());
    eval(raw);
};

Content.prototype.onInitCustomLogic = function(logic){
    this.customLogic_ = logic;
};



new Content();
