/**
 *
 * @param {string} action
 * @param {*=} opt_data
 * @constructor
 * @implements {SendMessageInterface}
 */
function SendMessage(action, opt_data) {
    this.action_ = action;
    this.data_ = opt_data ? opt_data : '';
}

/**
 *
 * @return {string}
 */
SendMessage.prototype.getAction = function () {
    return this.action_;
};

/**
 *
 * @return {*}
 */
SendMessage.prototype.getData = function () {
    return this.data_;
};


SendMessage.prototype.toJSON = function () {
    var data = {};
    data[SendMessage.ACTION_NAME] = this.action_;
    data[SendMessage.DATA_NAME] = this.data_;
    return data;
};

/**
 *
 * @param {Object} obj
 * @return {SendMessage}
 */
SendMessage.make = function(obj){
    return new SendMessage(
        obj[SendMessage.ACTION_NAME],
        obj[SendMessage.DATA_NAME]
    );
};

SendMessage.ACTION_NAME = 'action';
SendMessage.DATA_NAME = 'data';
