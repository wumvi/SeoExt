/**
 *
 * @param {number} width
 * @param {number} height
 * @param {number} beginProc
 * @param {number} endProc
 * @constructor
 */
function ScreenResolution(width, height, beginProc, endProc) {
    this.width_ = width;
    this.height_ = height;
    this.beginProc_ = beginProc;
    this.endProc_ = endProc;
}

/**
 *
 * @return {number}
 */
ScreenResolution.prototype.getWidth = function () {
    return this.width_;
};


/**
 *
 * @return {number}
 */
ScreenResolution.prototype.getHeight = function () {
    return this.height_;
};


/**
 *
 * @return {number}
 */
ScreenResolution.prototype.getBeginProc = function () {
    return this.beginProc_;
};


/**
 *
 * @return {number}
 */
ScreenResolution.prototype.getEndProc = function () {
    return this.endProc_;
};
